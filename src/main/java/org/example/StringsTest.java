package org.example;
import org.apache.commons.lang3.StringUtils;
import java.util.Scanner;

public class StringsTest {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите текст для вывода строки с последнего символа по первый:");
        String userText = scanner.nextLine();
        System.out.println(StringUtils.reverse(userText));

        System.out.println("Введите текст для смены регистра каждого символа:");
        String userText2 = scanner.nextLine();
        System.out.println(StringUtils.swapCase(userText2));

    }
}
